class Config:
    SECRET_KEY = "gksdideogkrrysalt"
    DEBUG = False


class DevelopmentConfig(Config):
    DEBUG = True
    SEND_FILE_MAX_AGE_DEFAULT = 0


class TestingConfig(Config):
    DEBUG = True
    TESTING = True


class ProductionConfig(Config):
    DEBUG = False


config_by_name = dict(development=DevelopmentConfig, test=TestingConfig, production=ProductionConfig, default=Config)
key = Config.SECRET_KEY
