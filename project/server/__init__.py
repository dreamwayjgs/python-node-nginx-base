import os
from flask import Flask
from .config import config_by_name


def create_app():
    # TODO: Test: pass or Die

    # Flask server init
    app = Flask(
        __name__,
        static_url_path="",
        static_folder="static/dist",
        template_folder="templates",
    )
    config_mode = os.getenv("FLASK_ENV")
    app.config.from_object(config_by_name.get(config_mode, config_by_name["default"]))

    # blueprint for non-auth parts of app
    from .main import main as main_blueprint

    app.register_blueprint(main_blueprint)

    return app
