Vscode 에서 docker remote environment 작업하기
=============

[**VSCode 공식 문서**](https://code.visualstudio.com/docs/remote/containers)

[**VSCode Docker 고급**](https://code.visualstudio.com/docs/remote/containers-advanced)

1. 공식 문서의 Docker extension 을 설치합니다.
2. 고급 문서의 Connecting to multiple containers at once 을 참조하세요.


세부 순서 (2번 출처)
1. Run Remote-Containers: Open Folder in Container... from the Command Palette (F1) and select the 'python' folder.
2. Run Remote-Containers: Open Folder in Container... from the Command Palette (F1) and select the 'node' folder.

3. 만약 기본값 (flask, parcel) 을 사용하고 싶다면
4. python workspace에서
<pre><code>pip install -r requirements.txt</code></pre>

5. node workspace에서
```
yarn add global parcel-bundler
yarn
```

DONE!

Node 또는 python 하나만 사용하려면 docker-compose.yml을 적절히 변경하고 사용하지 않는 폴더를 삭제하세요.

기본 nginx 포트 48080